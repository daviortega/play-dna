# play DNA

NodeJS cli app to convert DNA into sounds, `T` is consider a pause.

## Install

```
git clone git@gitlab.com:daviortega/play-dna.git
cd play-dna
npm install
```

## Play

There is a bit of 720 bases of the Human Genome in the refSeq coded in it. To play with the duration of 0.1s each note:

```
npm start
```
