'use strict'

const duration = 0.1

const dna = 'CTCTGGTTTGTCATTAGTGGTATTCTGAAAAGCTTAAGAAAAAAGAATCATTGAGTAGAAAGTGAACATTTTAAGTTATTTTCCTTTTTCATTGGCAATTAGTTAACATGTAAAATCTTTTCCATTAAACTGTATAACTTTTAATAACTATATATTATATATGGGATAAAATATATGGACTGAATTGTGAGAATATAAATGCATCGAAAGACTCTGGTGTCATGAAAGCACAAGAATAAGGCTGGAGATGGTCCCAGATCCAAGATGTCCCAAAAGCCTTTTGTATCAGTTTCTCTATTTTTGGTATTTTGCATAATGCATGGATCCTTAGTTCAAATGAGGGACAAAGTTTCTCAGTCAGCCCCACTTCCTTTCTTTCTAACTCCTACCTTTCCCTTGCAGAGGAGGTAGTAGAGATTCTGGAATTGTCTATTTTTATGAATTCCATTATTTTGTCCATGGCATCTCTAATGAAAACAGGTTCTAGAATAAAGGAGTTGATTAGTCTGAACAGTACTAATTAACTACAAAATAAACGTTAGTGATCAGCCTCTTCCTCTATAAACAATGACCAATTAGACGTTTCCGTAATTCCATGTATTATGTATAGTACACTCTATAAATGTAAATGTAATGCTTGTCTAAAAAGTGCAATTTATTGTACATTGTCCCAACAAATGTTTACTTTTATAATCGTTATGAACTTGAATTGGATTAGTA'

const dnaBaseList = dna.split('')


const note2freq = new Map()
note2freq['A'] = 440
note2freq['C'] = 261.6
note2freq['G'] = 392

var Speaker = require('audio-speaker/stream');
var Generator = require('audio-generator/stream');

const makeSound = (note) => {
    return new Promise((resolve) => {
        Generator(function (time) {
            //panned unisson effect
            var τ = Math.PI * 2;
            return note !== 'T' ? [Math.sin(τ * time * note2freq[note])] : []
        }, {duration: duration})
        .on('end', () => {
            setTimeout(resolve, duration*1000)
        })
        .pipe(Speaker());
    })
}


const play = async () =>  {
    for (let i in dnaBaseList) {
        const note = dnaBaseList[i]
        console.log(`${note} - ${i}/${dnaBaseList.length}`)
        await makeSound(note)
    }
}
play()


